package pond;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Reboot utility for JVM.
 *
 * @author kappa
 */
public final class RestartJVM {

	private RestartJVM() {
		// not used
	}

	public static void main(String[] args) {
		if (restartJVM(args)) {
			System.exit(0);
		} else {
			TDDemo.main(args);
		}
	}

	/** Reboots the JVM on Mac OS X if we are not on the first thread! */
	public static boolean restartJVM(String[] args) {
		String osName = System.getProperty("os.name");

		// if not a mac return false
		if ((!osName.startsWith("Mac")) && (!osName.startsWith("Darwin"))) {
			return false;
		}
		RuntimeMXBean runtime = ManagementFactory.getRuntimeMXBean();
		// get current jvm process pid
		// Java <10: String pid = runtime.getName().split("@")[0];
		long pid = runtime.getPid();
		// get environment variable on whether XstartOnFirstThread is enabled
		String env = System.getenv("JAVA_STARTED_ON_FIRST_THREAD_" + pid);

		// if environment variable is "1" then XstartOnFirstThread is enabled
		if ("1".equals(env)) {
			return false;
		}

		// restart jvm with -XstartOnFirstThread
//		List<String> jvmArgs = new ArrayList<>(runtime.getInputArguments());
//		System.out.println(jvmArgs);
//		System.exit(0);
		String separator = System.getProperty("file.separator");
		String classpath = System.getProperty("java.class.path");
		String mainClass = System.getenv("JAVA_MAIN_CLASS_" + pid);
		String jvmPath = System.getProperty("java.home") + separator + "bin" + separator + "java";

		ArrayList<String> jvmArgs = new ArrayList<String>(128);
		jvmArgs.add(0, jvmPath);
		jvmArgs.add(1, "-XstartOnFirstThread");
		for(String s : ManagementFactory.getRuntimeMXBean().getInputArguments())
			jvmArgs.add(s.replaceAll("address=localhost:[0-9]+", "address=localhost:12345"));
		jvmArgs.add("-cp");
		jvmArgs.add(classpath);
		jvmArgs.add(mainClass);
		for (int i = 0; i < args.length; i++) {
			jvmArgs.add(args[i]);
		}

  System.out.println(String.join("\n", jvmArgs));
//  System.exit(0);

		// if we want console output via same JVM
		final boolean consoleOutputViaSameJVM = false;
		try {
			if (consoleOutputViaSameJVM) {
				// with console output: the current JVM will continue & show console output...
				ProcessBuilder processBuilder = new ProcessBuilder(jvmArgs);
				processBuilder.redirectErrorStream(true);
				Process process = processBuilder.start();

				InputStream is = process.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				String line;

				while ((line = br.readLine()) != null) {
					System.out.println(line);
				}

				process.waitFor();
			} else {
				// without console output: the current JVM will terminate!
				ProcessBuilder processBuilder = new ProcessBuilder(jvmArgs);
				processBuilder.inheritIO();
				Process process = processBuilder.start();
				Map<String, String> getenv = System.getenv();
				for (String s : getenv.keySet())
					System.out.println(s + ": " + getenv.get(s));
				Properties properties = System.getProperties();
				for (Object s : properties.keySet())
					System.out.println(s + ": " + getenv.get(s));
				System.out.println(properties);
				System.exit(process.waitFor());

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}
}