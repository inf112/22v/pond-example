package pond.view;

import pond.model.PondObject;
import turtleduck.canvas.Canvas;
import turtleduck.geometry.Point;
import turtleduck.turtle.Turtle;

public interface Viewable<T> {
	/**
	 * Draw the object on the screen.
	 * 
	 * The object must position the drawing itself.
	 * 
	 * @param obj The object to draw
	 * @param canvas A canvas to be drawn on
	 * @param turtle A turtle object used for the drawing
	 */
	void draw(T obj, Canvas canvas, Turtle turtle);
}
