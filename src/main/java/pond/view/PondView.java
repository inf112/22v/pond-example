package pond.view;

import java.util.HashMap;
import java.util.Map;

import pond.model.Pond;
import pond.model.PondObject;
import turtleduck.canvas.Canvas;
import turtleduck.turtle.Turtle;

public class PondView implements Viewable<Pond> {
	Map<String, Viewable<? extends PondObject>> viewMap = new HashMap<>();

	public PondView() {
		viewMap.put("duck", new DuckView());
		viewMap.put("frog", new FrogView());
		
	}

	@Override
	public void draw(Pond pond, Canvas canvas, Turtle turtle) {
		// TODO Auto-generated method stub
		for (PondObject o : pond.objects()) {
			String viewRef = o.viewRef();
			Viewable<PondObject> viewable = (Viewable<PondObject>) viewMap.get(viewRef);
			if (viewable == null)
				viewable = new DefaultView();

			viewable.draw(o, canvas, turtle.spawn());
		}
	}
}
