package pond.view;

import pond.model.Duck;
import pond.model.PondObject;
import turtleduck.canvas.Canvas;
import turtleduck.colors.Colors;
import turtleduck.geometry.Direction;
import turtleduck.geometry.Point;
import turtleduck.turtle.Turtle;

public class DuckView implements Viewable<PondObject> {
	/**
	 * @param turtle
	 */
	@Override
	public void draw(PondObject obj, Canvas canvas, Turtle turtle) {
	 Point pos = obj.position();
		// go to our current coordinates
		canvas.color(Colors.BROWN);
		canvas.drawEllipse(pos, obj.getWidth(), obj.getHeight());
		Point head = pos.add(Direction.absolute(30), obj.getWidth()/2);
		canvas.drawEllipse(head, .6*obj.getWidth(), .6*obj.getHeight());
		//turtle.jumpTo(pos.getX(), pos.getY());
		// draw an ellipse, filled with bodyColor
		//turtle.pen(Colors.BROWN).strokeOnly();
		//for(int i = 0; i < 36; i++) {
		//	turtle.draw(5).turn(10);
	///	}
		//turtle.done();
/*		turtle.shape().ellipse().width(getWidth()).height(getHeight()).fillPaint(bodyColor).fill();
		// jump to a position 50*size away at 30° angle, for drawing the head
		turtle.turn(30);
		turtle.jump(getWidth()/2);
		// turn back so the head will be straight
		turtle.turn(-30);
		// draw head as ellipse with headColor
		turtle.shape().ellipse().width(getHeight()).height(0.6*getHeight()).fillPaint(headColor).fill();
		// restore painter state
		turtle.restore();
		drawHealth(turtle);
*/
	}

}
