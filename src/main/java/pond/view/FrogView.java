package pond.view;

import pond.model.Frog;
import turtleduck.canvas.Canvas;
import turtleduck.colors.Color;
import turtleduck.colors.Colors;
import turtleduck.geometry.Direction;
import turtleduck.geometry.Point;
import turtleduck.shapes.Ellipse.EllipseBuilder;
import turtleduck.turtle.Turtle;

public class FrogView implements Viewable<Frog> {


	private double jumpStageA;
	private Color bodyColor;
	private double size;
	private double croakStage;
	private double stepVariant;
	private int tongue;
	private Color headColor;
	private double jumpStageB;

	/**
	 * @param turtle
	 */
	@Override
	public void draw(Frog frog, Canvas canvas, Turtle turtle) {
		turtle.jumpTo(frog.position().add(0,3* frog.getHeight() * frog.jumpStageA * (0.5 + frog.stepVariant)));
		turtle.turnTo(-10);
		jumpStageA = frog.jumpStageA;
		jumpStageB = frog.jumpStageB;
		bodyColor = frog.getPrimaryColor();
		headColor = bodyColor.darker();
		croakStage = frog.croakStage;
		size = frog.size();
		stepVariant = frog.stepVariant;
		tongue = frog.tongue;
		drawLeg(frog, turtle.spawn().turn(5).color(bodyColor.darker()), jumpStageA);

		turtle.spawn(t -> {
			t.turn(25 - 10 * jumpStageA);
			t.jump(.75 * frog.getWidth() / (2.2 - 0.4 * jumpStageA));
			t.turn(-25 + 10 * jumpStageA);
			Point point = t.point();
			Direction direction = t.direction();
			canvas.ellipse().at(point).width(frog.getHeight() * croakStage * 1.).height(frog.getHeight() * croakStage * 1.)
					.fill(headColor.mix(Colors.YELLOW, croakStage / 2)).fill();
		});

		// draw an ellipse, filled with bodyColor
		turtle.spawn(t -> {
			Point point = t.point();
//			canvas.ellipse().at(point).width(frog.getWidth()).height(frog.getHeight()).fill(bodyColor).fill();
		});
		turtle.spawn(t -> {
			t.turn(-170 + 40 * jumpStageA);
			t.jump(frog.getWidth() / 2).color(bodyColor).fillOnly();
			t.turn(-120).draw(frog.getWidth() / 7).turn(-45).draw(frog.getWidth() / 3).turn(-10).draw(frog.getWidth() / 5).turn(-10)
					.draw(frog.getWidth() / 5);
			Point point = t.point();
			Direction direction = t.direction();
			drawArm(frog, t.spawn().color(bodyColor.darker().darker()).strokeOnly().turn(0), jumpStageA);
			t.turn(-60).draw(frog.getWidth() / 10).turn(-70).draw(frog.getWidth() / 7).turn(-20).draw(frog.getWidth() / 3).turn(-30)
					.draw(frog.getWidth() / 3);
			t.turn(-30).draw(frog.getWidth() / 7);
			t.turn(45 * jumpStageA * (0.5 + stepVariant));
//		});
		// jump to a position 50*size away at 30° angle, for drawing the head
//		turtle.spawn(t -> {
//			t.turn(25 - 15 * jumpStageA);
//			t.jump(frog.getWidth() / (2 - 0.4 * jumpStageA));
			// turn back so the head will be straight
			t.jumpTo(point).turnTo(direction);
			canvas.ellipse().at(point).width(frog.getHeight()).height(0.6 * frog.getHeight()).fill(headColor).fill();
			if (tongue > 0) {
				double l = Math.sin(Math.toRadians(tongue));
				t.spawn().color(Colors.PINK).stroke(Colors.RED, 3).jump(.5 * frog.getHeight()).draw( 5 * l * frog.getHeight()).done();
			}
			t.turn(120).jump(.31 * frog.getHeight());
			point = t.point();
			EllipseBuilder e = canvas.ellipse().at(point).width(.2 * frog.getHeight()).height(0.2 * frog.getHeight()).fill(Colors.YELLOW.alpha(0.9));
			canvas.ellipse().at(point).width(.3 * frog.getHeight()).height(0.3 * frog.getHeight()).fill(bodyColor).fill();
			e.fill();
			canvas.ellipse().at(point.add(.2, 0)).width(.02 * frog.getHeight()).height(0.1 * frog.getHeight()).fill(Colors.BLACK)
					.fill();
			if (frog.blinking > 0)
				canvas.ellipse().at(point).width(.3 * frog.getHeight()).height(0.3 * frog.getHeight()).fill(bodyColor).fill();

			// draw head as ellipse with headColor
			// turtle.shape().ellipse().width(frog.getHeight()).height(0.6 *
			// frog.getHeight()).computedFill(headColor).fill();
			// restore painter state
		});
		drawLeg(frog, turtle.spawn().color(bodyColor.darker().darker()), jumpStageA);

		// draw an ellipse, filled with bodyColor

	}

	public void drawArm(Frog frog, Turtle turtle, double jumpStage) {
		Color c = bodyColor.mix(Colors.BROWN, .15).darker();
		Color c2 = bodyColor.mix(Colors.FORESTGREEN, .25);
		c = c.mix(c2, .2);

		turtle.turn(-150);
		turtle.turn(15).jump(frog.getWidth() / 10).turn(-15);
		turtle.penChange().stroke(c, 3 * size).done();
		turtle.draw(frog.getWidth() / 3);

		c = c.mix(c2, .2);
		turtle.turn(125 - 140 * jumpStage);
		turtle.penChange().stroke(c, 3 * size).done();
		turtle.draw(frog.getWidth() / 3);

		c = c.mix(c2, .2);
		turtle.penChange().stroke(c, 2 * size).done();
		turtle.spawn().turn(-15 + 35 * jumpStage).draw(frog.getWidth() / 8);
		turtle.spawn().turn(-105 + 135 * jumpStage).draw(frog.getWidth() / 12);
	}

	public void drawLeg(Frog frog, Turtle turtle, double jumpStage) {
		// Math.abs((step % 100) - 50) / 50.0;
		// legs

		Color c = bodyColor.mix(Colors.BROWN, .15).darker();
		Color c2 = bodyColor.mix(Colors.FORESTGREEN, .25);
		c = c.mix(c2, .2);

		turtle.turn(-170 + 40 * jumpStage);
		turtle.penChange().stroke(c, 8 * size).done();
		turtle.jump(frog.getWidth() / 2.5);
		turtle.turn(-(170 + 40 * jumpStage));

		c = c.mix(c2, .2);
		turtle.turn(-25 - (160 * jumpStage));
		turtle.penChange().stroke(c, 6 * size).done();
		turtle.draw(frog.getWidth() / 3);

		c = c.mix(c2, .2);
		turtle.turn(-160 + (165 * jumpStage));
		turtle.penChange().stroke(c, 5 * size).done();
		turtle.draw(frog.getWidth() / 3);

		c = c.mix(c2, .2);
		turtle.turn(165 - (160 * jumpStage));
		turtle.penChange().stroke(c, 4 * size).done();

		turtle.draw(frog.getWidth() / 6);

		c = c.mix(c2, .2);
		turtle.turn(0 - 45 * jumpStage);
		for (int i = -1; i < 2; i++) {
			int k = i;
			Color col = c.mix(c2, .2);
			turtle.spawn(t -> {
				t.turn(k * 3 * jumpStageB);
				t.penChange().stroke(col, 1.5 * size).done();

				t.draw(frog.getWidth() / 6);
				t.turn(-k * 1 * jumpStageB + 115 * jumpStage * (1 - jumpStage));
				t.penChange().stroke(col, 1.5 * size).done();

				t.draw(frog.getWidth() / 6);
			});
		}

	}

}
