package pond.model;

import turtleduck.colors.Color;
import turtleduck.colors.Colors;

public class Duckling extends Duck {
	public Duckling(MyPoint pos, double size) {
		super(pos, size);
	}

	protected Color getPrimaryColor() {
		return Colors.YELLOW;
	}
	
	public void step() {
		pos.move(2, 0);
	}
}
