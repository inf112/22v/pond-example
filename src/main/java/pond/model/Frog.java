package pond.model;

import turtleduck.colors.Color;
import turtleduck.colors.Colors;

/**
 * A duck class – recipe for making duck objects.
 * 
 * Describes the common aspects of and behaviour of duck objects (in methods)
 * and the individual differences between ducks (in field variables).
 */
public class Frog extends PondDweller {
	// Dette er datastrukturen (sett av feltvariabler) som lagrer tilstanden til
	// andeobjektene:
	// "private" betyr at vi ønsker at dette skal være skjult, og at feltvariablene
	// ikke kan
	// endres eller leses av objekter (av andre klasser)
	public boolean jumping = false;
	public boolean croaking = false;
	public int step = 0;
	public double linearStep;
	public double jumpStageA, jumpStageB;
	public double croakStage;
	public double stepVariant;
	public int blinking;
	public int tongue;

	public Frog() {
		this(50, 20);
	}

	public Color getPrimaryColor() {
		return Colors.GREEN;
	}

	public double size() {
		return size;
	}

	public Frog(double x, double y) {
		this.pos = new MyPoint(x, y);
		this.size = 1 + Math.max(0, Math.pow(Pond.random.nextGaussian() / 2, 2));
		this.size = 1;
	}

	public Frog(MyPoint pos, double size) {
		this.pos = pos;
		this.size = size;
	}

	public Frog(double x, double y, double size) {
		this(x, y);
		this.size = size;
	}

	// Dette er definisjonen av metodene som utgjør oppførselen til andeobjektene
	// – brukes til å observere og manipulere endene

	// "public" betyr at objekter av andre klasser har lov å kalle disse metodene

	public void step(Pond world) {
		// i stedet for vanlig Java-valg av hvilken kode som blir kjørt,
		// så vil vi kjøre koden som er i PondDweller (superklassen)
		// (hvis vi sier bare step() eller this.step() – så vil Java se på klassen
		// til objektet, og finne metoden der, eller i en superklasse)
		super.step();
		int slow = 0;
		if (jumping) {
			int speed = (int) (1 + 3 * stepVariant);
//			health = health * 0.999;
			double stepLength = (3 * speed * (1 + size));
			linearStep = Math.min(1.0, step % stepLength / (stepLength - 1));
			jumpStageB = Math.pow(Math.sin(3 * Math.PI / 2 - 2 * Math.PI * linearStep), 1);
			// jumpStageA *= jumpStageA;
			jumpStageA = linearStep > .25 && linearStep < .75 ? (1 + jumpStageB) / 2
					: (1 + (Math.sin(Math.PI * (jumpStageB) / 2))) / 2;
			jumpStageA = (1 + (Math.sin((1 + 0.7 * Math.abs(linearStep - 0.5)) * Math.PI * (jumpStageB) / 2))) / 2;
			jumpStageA -= 0.073;
			if (linearStep < 1.0) {
				pos.move((getWidth() / (1 + 1 * stepVariant)) * jumpStageA, 0);
			} else {
				jumping = false;
			}

			step++;
		} else if (croaking) {
			int speed = (int) (slow + 10 + 3 * stepVariant);
			linearStep = Math.min(1.0, step % (10 * speed) / (9.0 * speed));
			croakStage = Math.sin(Math.sin(Math.sin(Math.PI * linearStep))); // +
																				// Math.abs(Math.sin(slow*Math.PI*linearStep))*.05;
			if (linearStep >= 1.0)
				croaking = false;
			step++;
		} else if (tongue > 0) {
			tongue -= 15;
		} else if (Pond.random.nextInt((int) (1 + 10 * size)) == 0) {
			stepVariant = Pond.random.nextDouble();
			if (Pond.random.nextInt(5) < size)
				croaking = true;
			else
				jumping = true;
			step = 0;
		} else if (Pond.random.nextInt((int) (1 + 100 * size)) == 0) {
			tongue = 180;
		}

		if (Pond.random.nextInt((int) (1 + 50 * size)) == 0) {
			blinking = 3;
		} else if (blinking > 0) {
			blinking--;
		}
		if (pos.getX() > world.width() / 2)
			pos.move(-world.width(), 0);
		if (pos.getX() < -world.width() / 2)
			pos.move(world.width(), 0);
		if (pos.getY() > world.height() / 2)
			pos.move(0, -world.height());
		if (pos.getY() < -world.height() / 2)
			pos.move(0, world.height());
	}

	/** @return The duck's width */
	public double getWidth() {
		return 1.5*70 * size;
	}

	/** @return The duck's height */
	public double getHeight() {
		return 1.5*30 * size;
	}

	/** @return Current X position */
	public double getX() {
		return pos.getX();
	}

	/** @return Current Y position */
	public double getY() {
		return pos.getY();
	}

	/** @return Current X movement */
	public double getDeltaX() {
		// TODO
		return 0;
	}

	/** @return Current Y movement */
	public double getDeltaY() {
		// TODO
		return 0;
	}

	@Override
	public String viewRef() {
		return "frog";
	}

}
