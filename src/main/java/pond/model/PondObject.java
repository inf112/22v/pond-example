package pond.model;

import turtleduck.canvas.Canvas;
import turtleduck.geometry.Point;
import turtleduck.turtle.Turtle;

public interface PondObject {
	/**
	 * Do one time step of actions for the object
	 */
	void step(Pond world);
	
	String viewRef();
	
	Point position();

	double getWidth();
	
	double getHeight();
	
	double getX();
	double getY();
}
