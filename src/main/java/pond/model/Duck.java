package pond.model;


import turtleduck.colors.Color;
import turtleduck.colors.Colors;

/** A duck class – recipe for making duck objects.
 * 
 * Describes the common aspects of and behaviour of duck objects (in methods)
 * and the individual differences between ducks (in field variables). */
public class Duck extends PondDweller  {
	// Dette er datastrukturen (sett av feltvariabler) som lagrer tilstanden til andeobjektene:
	// "private" betyr at vi ønsker at dette skal være skjult, og at feltvariablene ikke kan
	// endres eller leses av objekter (av andre klasser)
	private boolean swimming = true;
	private int stepCount = 0;
	public Duck() {
		this(500, 200);
	}
	
	public void step(Pond world) {
		if(stepCount++ > 50) {
			stepCount = 0;
//			world.objs.add(new Duckling(new MyPoint(pos.getX(), pos.getY()), (Pond.random.nextDouble()+1)*size/3.0));
		}
	}
	
	protected Color getPrimaryColor() {
		return Colors.BROWN;
	}
	public Duck(double x, double y) {
		this.pos = new MyPoint(x, y);
		this.swimming = x > 600;
		if(x > 600)
			this.swimming = true;
		else
			this.swimming = false;
	}
	public Duck(MyPoint pos, double size) {
		this.pos = pos;
		this.size = size;
	}
	
	public Duck(double x, double y, double size) {
		this(x,y);
		this.size = size;
	}
	
	
	
	/**
	 * @return The duck's width 
	 */
	public double getWidth() {
		return 100 * size;
	}

	/**
	 * @return The duck's height
	 */
	public double getHeight() {
		return 50 * size;
	}


	/**
	 * @return Current X position
	 */
	public double getX() {
		return pos.getX();
	}

	/**
	 * @return Current Y position
	 */
	public double getY() {
		return pos.getY();
	}
	
	/**
	 * @return Current X movement
	 */
	public double getDeltaX() {
		// TODO
		return 0;
	}

	/**
	 * @return Current Y movement
	 */
	public double getDeltaY() {
		// TODO
		return 0;
	}

	@Override
	public String viewRef() {
		return "duck";
	}


}
