package pond.testing;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

import pond.util.IGenerator;
import pond.util.generators.IntGenerator;
import pond.util.generators.LinkedListGenerator;
import pond.util.generators.ListGenerator;
import pond.util.generators.StringGenerator;

public class ListTest {
	private static IGenerator<Integer> intGen = new IntGenerator();
	private static IGenerator<List<Integer>> listGen = new ListGenerator<>(intGen);
	private int N = 10000;

	public static void main(String[] args) {
		List<List<Integer>> listList = listGen.generateEquals(3);

		for (List<Integer> list : listList)
			System.out.println(list);

	}

	@Test
	public void addTest1() {
		for (int i = 0; i < N; i++) {
			addAddsOneProperty(listGen.generate(), intGen.generate());
		}
	}

	@Test
	public void addTest2() {
		IGenerator<String> strGen = new StringGenerator();
		IGenerator<List<String>> listGen = new ListGenerator<>(strGen);

		for (int i = 0; i < N; i++) {
			addAddsOneProperty(listGen.generate(), strGen.generate());
		}
	}

	@Test
	public void addTest3() {
		IGenerator<List<List<Integer>>> llistGen = new LinkedListGenerator<>(listGen);

		for (int i = 0; i < N; i++) {
			addAddsOneProperty(llistGen.generate(), listGen.generate());
		}
	}

	private <T> void addAddsOneProperty(List<T> list, T elem) {
		int size = list.size();
		list.add(elem);
		assertEquals(size + 1, list.size());
	}

	@Test
	public void shuffleTest1() {
		for (int i = 0; i < N; i++) {
			List<Integer> list = listGen.generate();
			shuffleRetainsAllElements1(list);
			shuffleRetainsAllElements2(list);
		}
	}

	@Test
	public void shuffleTest2() {
		IGenerator<List<Integer>> listGen = new ListGenerator<>(intGen, 100, 200);

		for (int i = 0; i < N; i++) {
			List<List<Integer>> list = listGen.generateEquals(2);
			shuffleActuallyShuffles(list.get(0), list.get(1));
		}
	}

	private <T> void shuffleRetainsAllElements1(List<T> list) {
		int size = list.size();
		Collections.shuffle(list);
		assertEquals(size, list.size());
	}

	private <T> void shuffleRetainsAllElements2(List<T> list) {
		List<T> old = new ArrayList<>(list);
		int size = list.size();
		Collections.shuffle(list);
		assertEquals(size, list.size());

		for (T elem : old) {
			assertTrue(list.contains(elem), "Should still contain all elements from before shuffle");
		}
	}

	/**
	 * Might fail, even with perfect shuffle
	 * 
	 * @param list1
	 * @param list2
	 */
	private <T> void shuffleActuallyShuffles(List<T> list1, List<T> list2) {
		if (list1.equals(list2)) {
			Collections.shuffle(list2);
			assertNotEquals(list1, list2);
		}
	}

}
