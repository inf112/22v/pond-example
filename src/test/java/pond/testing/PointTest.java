package pond.testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import pond.model.MyPoint;

class PointTest {

	@Test
	void testConstructor() {
		MyPoint p = new MyPoint(200, 200);

		assertEquals(200, p.getX());
		assertEquals(200, p.getY());

		// best å teste med forskjellige verdier også
		p = new MyPoint(200, 300);

		assertEquals(200, p.getX());
		assertEquals(300, p.getY());

		// sjekk med litt varierte verdier
		p = new MyPoint(1200, 1300);
		assertEquals(1200, p.getX());
		assertEquals(1300, p.getY());

		p = new MyPoint(0, 0);
		assertEquals(0, p.getX());
		assertEquals(0, p.getY());

		// negative verdier er også ok her!
		p = new MyPoint(-120, -130);
		assertEquals(-120, p.getX());
		assertEquals(-130, p.getY());

	}

	@Test
	void testSetX() {
		MyPoint p = new MyPoint(200, 200);

		p.setX(400);
		assertEquals(400, p.getX());
		p.setX(1400);
		assertEquals(1400, p.getX());
		p.setX(-400);
		assertEquals(-400, p.getX());
		p.setX(0);
		assertEquals(0, p.getX());
	}

	@Test
	void testMove() {
		MyPoint p = new MyPoint(200, 200);

		assertEquals(200, p.getX());
		assertEquals(200, p.getY());
		p.move(5, 5);
		assertEquals(205, p.getX());
		assertEquals(205, p.getY());

		for (int dx = -100; dx < 100; dx++) {
			for (int dy = -100; dy < 100; dy++) {
				double x = p.getX();
				double y = p.getY();
				p.move(dx, dy);
				assertEquals(x + dx, p.getX());
				assertEquals(y + dy, p.getY());
			}
		}
	}
}
